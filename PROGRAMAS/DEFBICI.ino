

/// Variables //////////////////////////////////////////////////////////////////////////////////////////////////////////////

const int inputPin1 = 5;
const int inputPin2 = 6;
const int inputPin3 = 7;
const int inputPin4 = 8;
const int inputPin5 = 9;

int value1 = 00;
int value2 = 00;
int value3 = 00; 
int value4 = 00;
int value5 = 00;
int encoder_pin = 2;              //Pin 2, donde se conecta el encoder       
float rpm = 0;  // Revoluciones por minuto calculadas.
float rpm2=0;
const float rt=20/28;              //Relacion de transmision del nº de dientes del eje en el que se encuentra el encoder y en el que se pedalea

volatile int pulsos = 0;         // Número de pulsos leidos por el Arduino en un segundo
unsigned long timeold = 0;       // Tiempo anterior
unsigned int pulsesperturn = 3;  //Numero de pulsos por revolucion

int pinzumbador = 4;    // pin del zumbador
int frecuenciatono = 220;      // frecuencia correspondiente a la nota

////  Configuración del Arduino /////////////////////////////////////////////////////////
void setup()
{
   Serial.begin(57600); // Configuración del puerto serie  
   pinMode(encoder_pin, INPUT); // Configuración del pin nº2
   attachInterrupt(0, counter, RISING); // Configuración de la interrupción 0, donde esta conectado. 
   pulsos = 0;
   rpm = 0;
   timeold = 0;
  Serial.print("RPM: ");
  Serial.begin(9600);
  pinMode(inputPin1, INPUT);
  pinMode(inputPin2, INPUT);
  pinMode(inputPin3, INPUT);
  pinMode(inputPin4, INPUT);
  pinMode(inputPin5, INPUT);
  }
  
////  Programa principal ///////////////////////////////////////////////////////////////////////
 void loop(){
   value1 = digitalRead(inputPin1);  //lectura digital de pin
 
  //mandar mensaje a puerto serie en función del valor leido
  if (value1 == LOW) {
      Serial.println("11");
  }
  else {
    
      Serial.println("01");
  }
  
  value2 = digitalRead(inputPin2);  //lectura digital de pin
 
  //mandar mensaje a puerto serie en función del valor leido
  if (value2 == LOW) {
      Serial.println("12");
  }
  else {
      Serial.println("02");
  }
   value3 = digitalRead(inputPin3);  //lectura digital de pin
 
  //mandar mensaje a puerto serie en función del valor leido
  if (value3 == LOW) {
      Serial.println("13");
  }
  else {
      Serial.println("03");
  }
   
   value4 = digitalRead(inputPin4);  //lectura digital de pin
 
  //mandar mensaje a puerto serie en función del valor leido
  if (value4 == LOW) {
      Serial.println("14");
  }
  else {
      Serial.println("04");
  }
   
 
  value5 = digitalRead(inputPin5);  //lectura digital de pin
 
  //mandar mensaje a puerto serie en función del valor leido
  if (value5 == LOW) {
      Serial.println("15");
  }
  else {
      Serial.println("05");
  }
   
 
 delay(1000);
   if (millis() - timeold >= 1000){  // Se actualiza cada segundo
      noInterrupts(); // Desconectamos la interrupción para que no actué en esta parte del programa.
      rpm = 60*1000 *( pulsos / pulsesperturn ) /  (millis() - timeold); // Calculamos las revoluciones por minuto
      rpm2=rpm*20/28;
      //LIMITE DE VELOCIDAD A 1200 rpm
      if(rpm > 60)
      {
        tone(pinzumbador,frecuenciatono);    // inicia el zumbido
        delay(5000);                    
        noTone(pinzumbador);               // lo detiene a los 5 segundos
      }
      
      
       Serial.println(rpm2);
      timeold = millis(); // Almacenamos el tiempo actual.
     
      pulsos = 0;  // Inicializamos los pulsos.
      interrupts(); // Reiniciamos la interrupción
   }
  }
////Fin de programa principal //////////////////////////////////////////////////////////////////////////////////

///////////////////////////Función que cuenta los pulsos ///////////////////////////////////////////
 void counter(){
 pulsos++; } 
