import pygame
from random import choice
import sys

# ESTE JUEGO CONSISTE EN DESTRUIR LAS LETRAS QUE TE MARCA EL JUEGO, TIENE UN 
# DIDACTO QUE ES AYUDAR A DISTINGUIR LAS LETRAS CON UNA FORMA PARECIDA (d Y b)
# EL JUEGO ESTA CONTRUIDO PARA UNA BICICLETA REALIZADA POR MIS COMPAÑEROS
# EN EL MANILLAR DE ELLA SE ENCUENTRAN 3 BOTONES QUE VAN A SER NUESTROS CONTROLES
# EL BOTON CENTRAL SERVIRA PARA DISPARAR Y LOS OTROS DOS NOS SERVIRAN PARA
# DIRIGIR LA NAVA PARA LA DERECHA O PARA LA IZQUIERDA.

#CLASES-----------------------------------------------------------------------

class Marciano(pygame.sprite.Sprite):
    def __init__(self,imagen,ladox,ladoy,activo):
        pygame.sprite.Sprite.__init__(self)
        self.image = load_image(imagen,ladox , ladoy, True)
        self.rect = self.image.get_rect()
        self.clase = imagen
        self.activado=activo
        
    def mover(self, avance_x, avance_y):
            'avance del cuadrado en un cuadro (frame)'
            self.rect.topleft=(self.rect.x+avance_x, self.rect.y+avance_y)
            
class Nave(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = load_image("nave.png",50 , 90, True)
        self.rect = self.image.get_rect()
        
    def mover(self, avance_x, avance_y):
            'avance del cuadrado en un cuadro (frame)'
            self.rect.topleft=(self.rect.x+avance_x, self.rect.y+avance_y)

class Disparo(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = load_image("disparo.png",50 , 50, True)
        self.rect = self.image.get_rect()
        
    def mover(self, avance_x, avance_y):
            'avance del cuadrado en un cuadro (frame)'
            self.rect.topleft=(self.rect.x+avance_x, self.rect.y+avance_y)  
            

def load_image(filename,w,h, transparent=False):
        try: image = pygame.image.load(filename)
        except pygame.error, message:
                raise SystemExit, message
        image = image.convert()
        if transparent:
                color = image.get_at((0,0))
                image.set_colorkey(color)
        return (pygame.transform.scale(image, (w,h)))        

#VARIABLES---------------------------------------------------------------------

pygame.init()
pantalla = pygame.display.set_mode((720,650))      #SI SE QUISIERA UNA VENTANA
fondo = load_image("fondo.png",720,650)         #MAS GRANDE UNICAMENTE SE TIENE   
icon = pygame.image.load("marciano.png")     #QUE CAMBIAR EL 720 Y EL 650 QUE 
marcianos1 =[1,1,1,1,1,1,1,1,1,1,1,1]       #ESTAN A LA IZQUIERDA (NO ES 
marcianos2 =[1,1,1,1,1,1,1,1,1,1,1,1]       #RECOMENDABLE)
marcianos3 =[1,1,1,1,1,1,1,1,1,1,1,1]
marcianos4 =[1,1,1,1,1,1,1,1,1,1,1,1]
posini=0
disparo = Disparo()
nave=Nave()
nave.rect.x=330
nave.rect.y=500
disparo.rect.x=nave.rect.x+4
disparo.rect.y=nave.rect.y+10
fuente = pygame.font.Font(None,70)
puntuacion = 0
cant1=0
cant2=0
cant3=0
cant4=0
mover1=True
mover2=True
mover3=True
mover4=True
t=False
izq=False
der=False
mensajefin=False

#VARIABLES A CAMBIAR----------------------------------------------------------

#ESTOS VALORES SE PUEDEN CAMBIRA PARA QUE EL JUEGO SEA MAS SENCILLO, LAS DOS 
#VARIABLES CAMBIAN EL TAMAÑO DE LA LETRA, MIENTRAS QUE LA TERCERA CAMBIA LA
#SEPARACION ENTRE ELLAS Y LA CUARTA EL NUMERO DE LETRAS POR CADA FILA 

anchoLetra=70
altoLetra=70   
separacion=64    
longitud=11      
FPS = 50 #ESTA VARIABLE SE PUEDE MODIFICAR PAR AUMENTAR O DISMINUIR 
         #LA VELOCIDAD DE EL JUEGO (NO ES RECOMENDABLE MODIFICARLA)

#------------------------------------------------------------------------------

extremo1_1 = 0
extremo2_1 = longitud-1
extremo1_2 = 0
extremo2_2 = longitud-1
extremo1_3 = 0
extremo2_3 = longitud-1
extremo1_4 = 0
extremo2_4 = longitud-1

#AQUI SE DEFINEN DOS POSIBLES FASES: LA PRIMERA SERIA ENTRE LAS d Y b 
#(DESTRUIR UNA O OTRA SEGUN SE MUESTRE) Y LA SEGUNDA FASE SERIA ENTRE q Y p

QueDispara=choice(["d","p","b","q"])  #SI SE QUISIERAN PONER LETRAS DISTINTAS
if QueDispara=="b":                   #SOLO HARIA FALTA CAMBIAR LAS d Y b QUE 
    letra1="d.png"                    #SE VEN HASTA LA DELIMITACION POR LAS 
    letra2="b.png"                    #LAS OTRAS (PREVIAMENTE SE TENDRIAN QUE 
    modo = 1                          #INTRODUCIR UNA IMAGEN .PNG EN LA CARPETA
    mensini="Elimina las b"           #DEL PROYECTO CON EL NOMBRE nombre_letra.png)
elif QueDispara=="d":
    letra1="d.png"
    letra2="b.png"
    modo = 2
    mensini="Elimina las d"

#/////////////////////////////////////////////////////////////////////////////
elif QueDispara=="p" :                 # AL IGUAL QUE EN EL CASO ANTERIOR
    letra1="p.png"
    letra2="q.png"
    modo = 3
    mensini="Elimina las q"
else:
    letra1="p.png"
    letra2="q.png"
    modo = 4
    mensini="Elimina las p"

QueDispara=="p"
for i in range (longitud):   #EN ESTA PARTE DE EL CODIGO SE ELIGE ALEATORIAMENTE 
                             #LA LETRA QUE SE VA A MOSTRAR 
    l2=choice([letra1,letra2])
    marcianos2[i]=Marciano(l2,anchoLetra,altoLetra,True)
    marcianos2[i].rect.x=posini
    marcianos2[i].rect.y=-200
    if modo==1 or modo==3:
        if l2==letra2:
            cant2+=1
    else:
        if l2==letra1:
            cant2+=1
            
    l3=choice([letra1,letra2])
    marcianos3[i]=Marciano(l3,anchoLetra,altoLetra,True)
    marcianos3[i].rect.x=posini
    marcianos3[i].rect.y=-400
    if modo==1 or modo==3:
        if l3==letra2:
            cant3+=1
    else:
        if l3==letra1:
            cant3+=1
            
    l4=choice([letra1,letra2])
    marcianos4[i]=Marciano(l4,70,70,True)
    marcianos4[i].rect.x=posini
    marcianos4[i].rect.y=-600
    if modo==1 or modo==3:
        if l4==letra2:
            cant4+=1
    else:
        if l4==letra1:
            cant4+=1
        
    l1=choice([letra1,letra2])    
    marcianos1[i]=Marciano(l1,70,70,True)
    marcianos1[i].rect.x=posini
    marcianos1[i].rect.y=0
    if modo==1 or modo==3:
        if l1==letra2:
            cant1+=1
    else:
        if l1==letra1:
            cant1+=1
        
    posini=posini+separacion



#MAIN--------------------------------------------------------------------------   

#PARTE PRINCIPAL DE EL PROGRAMA

pygame.display.update()
clock = pygame.time.Clock()
pygame.key.set_repeat(True)
pygame.display.set_caption('MARCIANITOS')
pygame.display.set_icon(icon)

while True:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
            
        if event.type==pygame.MOUSEBUTTONDOWN:        #MOVIMIENTO JUGADOR
            if pygame.mouse.get_pressed()==(1,0,0):   #IZQ
                izq=True
                der=False
            if pygame.mouse.get_pressed()==(0,0,1):   #DER
                der=True
                izq=False
            if pygame.mouse.get_pressed()==(0,1,0):   #DISPARO
                t=True
                der=False
                izq=False

    #MOVIMIENTO LETRAS LATERAL            
    if marcianos1[extremo2_1].rect.x<720-70 and mover1==True:
        for i in range (longitud):
                marcianos1[i].mover(1,0)
        if not marcianos1[extremo2_1].rect.x<720-70:
            mover1=False
    if marcianos1[extremo1_1].rect.x>0-30 and mover1==False:
         mover1==False
         for i in range (longitud):
                marcianos1[i].mover(-1,0)
         if not marcianos1[extremo1_1].rect.x>0-30:
            mover1=True            
                    
    if marcianos2[extremo2_2].rect.x<720-70 and mover2==True:
        for i in range (longitud):
                marcianos2[i].mover(1,0)
        if not marcianos2[extremo2_2].rect.x<720-70:
            mover2=False
    if marcianos2[extremo1_2].rect.x>0-30 and mover2==False:
         mover2==False
         for i in range (longitud):
                marcianos2[i].mover(-1,0)
         if not marcianos2[extremo1_2].rect.x>0-30:
            mover2=True           

    if marcianos3[extremo2_3].rect.x<720-70 and mover3==True:
        for i in range (longitud):
                marcianos3[i].mover(1,0)
        if not marcianos3[extremo2_3].rect.x<720-70:
            mover3=False
    if marcianos3[extremo1_3].rect.x>0-30 and mover3==False:
         mover3==False
         for i in range (longitud):
                marcianos3[i].mover(-1,0)
         if not marcianos3[extremo1_3].rect.x>0-30:
            mover3=True           

    if marcianos4[extremo2_4].rect.x<720-70 and mover4==True:
        for i in range (longitud):
                marcianos4[i].mover(1,0)
        if not marcianos4[extremo2_4].rect.x<720-70:
            mover4=False
    if marcianos4[extremo1_4].rect.x>0-30 and mover4==False:
         mover4==False
         for i in range (longitud):
                marcianos4[i].mover(-1,0)
         if not marcianos4[extremo1_4].rect.x>0-30:
            mover4=True

       #MOVIMIENTO LETRAS DESCENDENTE
    activo1=marcianos1[extremo1_1].activado               
    activo2=marcianos2[extremo1_2].activado 
    activo3=marcianos3[extremo1_3].activado
    activo4=marcianos4[extremo1_4].activado
    if activo1:
        if not marcianos1[extremo1_1].rect.x>0-30:
            for i in range(longitud):
                marcianos1[i].mover(0,5)
                marcianos2[i].mover(0,5)
                marcianos3[i].mover(0,5)
                marcianos4[i].mover(0,5)
        if not marcianos1[extremo2_1].rect.x<720-71:
            for i in range(longitud):
                marcianos1[i].mover(0,5)
                marcianos2[i].mover(0,5)
                marcianos3[i].mover(0,5)
                marcianos4[i].mover(0,5)
    elif activo2:
        if not marcianos2[extremo1_2].rect.x>0-30:
            for i in range(longitud):
                marcianos2[i].mover(0,5)
                marcianos3[i].mover(0,5)
                marcianos4[i].mover(0,5)
        if not marcianos2[extremo2_2].rect.x<720-71:
            for i in range(longitud):
                marcianos2[i].mover(0,5)
                marcianos3[i].mover(0,5)
                marcianos4[i].mover(0,5)
    elif activo3:
        if not marcianos3[extremo1_3].rect.x>0-30:
            for i in range(longitud):
                marcianos3[i].mover(0,5)
                marcianos4[i].mover(0,5)
        if not marcianos3[extremo2_3].rect.x<720-71:
            for i in range(longitud):
                marcianos3[i].mover(0,5)
                marcianos4[i].mover(0,5)
    elif activo4:
        if not marcianos4[extremo1_4].rect.x>0-30:
            for i in range(longitud):
                marcianos4[i].mover(0,5)
        if not marcianos4[extremo2_4].rect.x<720-71:
            for i in range(longitud):
                marcianos4[i].mover(0,5)
    #BORRAR ENEMIGOS MISMA LINEA SI NO HAY LETRAS PUNTUABLES                 
    for i in range(longitud):
            if cant1==0:
                marcianos1[i].activado=False
            if cant2==0:
                marcianos2[i].activado=False
            if cant3==0:
                marcianos3[i].activado=False
            if cant4==0:
                marcianos4[i].activado=False
                mensajefin=True

    #MOSTRA POR PANTALLA ELEMENTOS
    pantalla.blit(fondo,(0,0))        
    for i in range(longitud):
        if marcianos1[i].activado==True:
            pantalla.blit(marcianos1[i].image, marcianos1[i].rect)
        if marcianos2[i].activado==True:
            pantalla.blit(marcianos2[i].image, marcianos2[i].rect)
        if marcianos3[i].activado==True:
            pantalla.blit(marcianos3[i].image, marcianos3[i].rect)
        if marcianos4[i].activado==True:
            pantalla.blit(marcianos4[i].image, marcianos4[i].rect)
            
    pantalla.blit(nave.image,nave.rect)
    marcador = fuente.render(str(puntuacion),1,(255,255,255))
    menfin = fuente.render(str("Puntos Finales:"),1,(255,255,255))
    if mensajefin==True:
        pantalla.blit(menfin,(50,250))
        pantalla.blit(marcador,(500,250))
        mensini=""
    else:
        pantalla.blit(marcador,(0,0))
    marcador = fuente.render(str(puntuacion),1,(255,255,255))
    inicio = fuente.render(str(mensini),1,(60,255,255))
    pantalla.blit(inicio,(0,600))
    
    if marcianos1[1].rect.y==400:
        for i in range(longitud):
            marcianos1[i].activado=False
    if marcianos2[1].rect.y==400:
        for i in range(longitud):
            marcianos2[i].activado=False
    if marcianos3[1].rect.y==400:
        for i in range(longitud):
            marcianos3[i].activado=False
    if marcianos4[1].rect.y==400:
        for i in range(longitud):
            mensajefin=True
            marcianos4[i].activado=False
    
    if izq==True:
        if nave.rect.x>0:
            nave.mover(-2,0)
    
    if der==True:
        if nave.rect.x<650:
            nave.mover(2,0)     
 
    if t==True:
        disparo.mover(0,-5)
        pantalla.blit(disparo.image,disparo.rect)
        if disparo.rect.y<-20:
            t=False
    if t==False:
         disparo.rect.x=nave.rect.x+4
         disparo.rect.y=nave.rect.y+10
#COLISIONES-------------------------------------------------------------------- 
    for i in range(longitud):
        if disparo.rect.colliderect(marcianos1[i]):
            if marcianos1[i].activado==True:
                if modo == 1 or modo==3:
                    if marcianos1[i].clase==letra2:
                        puntuacion = puntuacion+5
                        cant1-=1
                    else:
                        puntuacion = puntuacion-1
                elif modo==2 or modo==4:
                    if marcianos1[i].clase==letra1:
                        puntuacion = puntuacion+5
                        cant1-=1
                    else:
                        puntuacion = puntuacion-1
                marcianos1[i].activado=False
                disparo.rect.y=-700
                if not(extremo1_1==extremo2_1):
                    if i == extremo1_1:
                        while marcianos1[extremo1_1].activado!=True:
                            extremo1_1+=1
                    if i ==extremo2_1:
                        while marcianos1[extremo2_1].activado!=True:
                           extremo2_1-=1
    

        if disparo.rect.colliderect(marcianos2[i]):
            if marcianos2[i].activado==True and marcianos2[i].rect.y>0:
                if modo == 1 or modo==3:
                        if marcianos2[i].clase==letra2:
                            puntuacion = puntuacion+5
                            cant2-=1
                        else:
                            puntuacion = puntuacion-1
                elif modo==2 or modo==4:
                        if marcianos2[i].clase==letra1:
                            puntuacion = puntuacion+5
                            cant2-=1
                        else:
                            puntuacion = puntuacion-1
                marcianos2[i].activado=False
                disparo.rect.y=-2000
                if not(extremo1_2==extremo2_2):
                    if i == extremo1_2:
                        while marcianos2[extremo1_2].activado!=True:
                            extremo1_2+=1
                    if i == extremo2_2:
                        while marcianos2[extremo2_2].activado!=True:
                            extremo2_2-=1
        

        if disparo.rect.colliderect(marcianos3[i]):
            if marcianos3[i].activado==True and marcianos3[i].rect.y>0:
                if modo == 1 or modo==3:
                        if marcianos3[i].clase==letra2:
                            puntuacion = puntuacion+5
                            cant3-=1
                        else:
                            puntuacion = puntuacion-1
                elif modo==2 or modo==4:
                        if marcianos3[i].clase==letra1:
                            puntuacion = puntuacion+5
                            cant3-=1
                        else:
                            puntuacion = puntuacion-1
                marcianos3[i].activado=False
                disparo.rect.y=-2000
                if not(extremo1_3==extremo2_3):
                    if i == extremo1_3:
                        while marcianos3[extremo1_3].activado!=True:
                            extremo1_3+=1
                    if i == extremo2_3:
                        while marcianos3[extremo2_3].activado!=True:
                            extremo2_3-=1
        
                
        if disparo.rect.colliderect(marcianos4[i]):
            if marcianos4[i].activado==True and marcianos4[i].rect.y>0:
                if modo == 1 or modo==3:
                        if marcianos4[i].clase==letra2:
                            puntuacion = puntuacion+5
                            cant4-=1
                        else:
                            puntuacion = puntuacion-1
                elif modo==2 or modo==4:
                        if marcianos4[i].clase==letra1:
                            puntuacion = puntuacion+5
                            cant4-=1
                        else:
                            puntuacion = puntuacion-1
                marcianos4[i].activado=False
                disparo.rect.y=-2000
                if not(extremo1_4==extremo2_4):
                    if i == extremo1_4:
                        while marcianos4[extremo1_4].activado!=True:
                            extremo1_4+=1
                    if i == extremo2_4:
                        while marcianos4[extremo2_4].activado!=True:
                            extremo2_4-=1

    pygame.display.update()
    
pygame.time.delay(3000)
pygame.quit()
