

import random
import pygame #Dudas sobre su instalacion resueltas en https://www.pygame.org/wiki/GettingStarted

N_MAX_ENEMIGOS=10 #Numero maximo de enemigos creados por el programa a la vez. NO controlara directamente el numero de enemigos. Debe ser mayor o igual a 3 o dara error en la funcion game.acciones_enemigas, apartado game over


#Todas los sonidos e imagenes cargadas deben estar en la misma carpeta que el codigo a ejecutar (este codigo)
##################################
##################################

# Cuatro variables globales cuya unica finalidad es facilitar el CAMBIO DE DIFICULTAD del juego
                                            
MOVIMIENTO_JUGADOR = 4  #Velocidad, en pixeles, del jugador, por cada tecla pulsada y el refrescar la pantalla. Se debe buscar un consenso entre velocidad y manioabrabilidad
MOVIMIENTO_ENEMIGO = 6  #Velocidad, en pixeles, de los enemigos por cada vez que se refresque la pantalla (si interesa modificar este tiempo, fps, ver
                       #la funcion reloj.tick(50) ubicada practicamente al final del codigo, donde 50 son los fps)

INTERVALO_ENEMIGOS_SUPERIOR = 1350 #Tiempo MAXIMO, en milisegundos, entre la aparicion de un enemigo u otro. 
INTERVALO_ENEMIGOS_INFERIOR = 1000 #Tiempo MINIMO, en milisegundos, entre la aparicion de un enemigo u otro. Debe tener un
                                  #valor lo suficientemente alto para que no se solapen los vehiculos y tampoco compliquen demasiado el juego
                        
# Otra manera de facilitar el cambio de dificultad del juego es cambiar la anchura y altura del jugador: para ello modificar los numeros, 60 y 120, de
#pygame.transform.scale(self.image, (60,120)), funcion ubicada en'class Player( ' 

# Se pueden cambiar la anchura y altura cada enemigo de manera individual, para ello modificar las variables self.ancho y self.largo en 'class Game( '

# Se puede cambiar el numero de vidas, pero teniendo en cuenta que no se crearon sprites para mas de 3 vidas.
#si se quieren aNadir mas vidas ver la variable 'self.num_vidas' en 'class Game( '. Si se quieren aNadir mas sprites de vidas ver 'class Sprites( '

##################################
##################################

MOVIMIENTO_PIXELES_CARRETERA=8 #Un numero menor hara que parezca que se mueve mas lento, uno mayor que se mueva mas rapido

SCREEN_WIDTH = 744 #TamaNos de la pantalla, en pixeles
SCREEN_HEIGHT = 656 #TamaNos de la pantalla, en pixeles

TIEMPO_ENTRE_ENEMIGOS = pygame.USEREVENT  #Eventos de pygame, seran luego usados para crear timers
INMUNIDAD_COLISION = pygame.USEREVENT+1
PARPADEO = pygame.USEREVENT + 2
PUNTAJE = pygame.USEREVENT + 3 
MOVIMIENTO_CARRETERA = pygame.USEREVENT + 4

NEGRO = (0, 0, 0) #Codigo hexadecimal del negro, sera usado para colorear las letras de la puntuacion

#NOTA: En pygame los ejes X e Y empiezan en la parte superior izquierda de la pantalla, como (0,0)#
#siendo el eje positivo la derecha de la pantalla y la parte baja de la pantalla desde (0,0)

#NOTA2: Todas las unidades que afectan a los sprites son medidas en pixeles
            
#Jugador         
class Player(pygame.sprite.Sprite):
    def __init__(self):
        
        super().__init__()    
        
        self.image = pygame.image.load("Moto.png") #Carga la imagen del jugador, la moto
        self.image = pygame.transform.scale(self.image, (60,120)) #Escala la imagen del jugador. 60 es su ancho, 120 su largo
        
        self.tamano = self.image.get_rect().size #Adquiere el tamaNo de la imagen en pixeles, no tiene especial interes, solo se usara para ayudar a centrar su posicion
                                                
        
        self.rect = self.image.get_rect()   #Permitira acceder a las variables que controlan la posicion de la imagen
        self.rect.x = SCREEN_WIDTH/2  - self.tamano[0]*2.5 #Posicion inicial del jugador, recta X
        self.rect.y = SCREEN_HEIGHT - self.tamano[1]*1.1   - 20 
        

     
    def move(self, mov_horizontal):  #Controla como se mueve el jugador
    
        if not ((self.rect.x >=440- self.tamano[0]/2.2 and  mov_horizontal>0) or (self.rect.x <= 50 - self.tamano[0]/4.5 and  mov_horizontal<0)):  
            self.rect.x += mov_horizontal #Cada tecleo movera al jugador un numero de pixeles determinado por 'mov_horizontal'
            
        #El 'if not' permite realizar el movimiento del jugador siempre que no se intente salir de los margenes de la pantalla
            
 
            
#Enemigos
class Enemy(pygame.sprite.Sprite):
 
    def __init__(self, foto, tamano_x, tamano_y):
        
        super().__init__()
        
        self.image = pygame.image.load(foto)  #Carga el contenido de la variable 'foto' que contendra el nombre de la imagen del enemigo
        self.image = pygame.transform.scale(self.image, (tamano_x,tamano_y)) #Escala la imagen
        
        self.tamano = self.image.get_rect().size #Adquiere el tamaNo de la imagen en pixeles, no tiene especial interes, solo se usara para ayudar a centrar su posicion
        self.rect = self.image.get_rect() #Permitira acceder a las variables que controlan la posicion de la imagen
        
        self.pausado = 1  #Esta variable mantendra a los enemigos quietos. Se mantendra activa al inicio del juego y hasta que no pase el tiempo de reparacion del enemigo
        
        
        
        
    def move(self, movimiento_enemigo):
 
        self.rect.y += movimiento_enemigo #Velocidad de movimiento de enemigos
 
        
    def colision(self, jugador):
        if self.rect.colliderect(jugador.rect): #Hizo colision con el jugador? Si es asi la funcion regresa 1
            return 1       
            


class Sprites(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()  
        self.image= pygame.image.load("vida3.png") #Carga una imagen, que muestra 3 vidas
        self.tamano = self.image.get_rect().size 

        self.rect = self.image.get_rect() #   Permitira acceder a las variables que controlan la posicion de la imagen
        self.rect.x = SCREEN_WIDTH - self.tamano[0] + 6 #Posicion de la imagen
        self.rect.y = 0 
        
    
    def recarga(self, numero_vidas): #Ira cargando las imagenes que muestran el numero de vidas a medida que estas se vayan perdiendo
        if numero_vidas == 2: 
            self.image=pygame.image.load("vida2.png")# Carga una imagen, que muestra 2 vidas
            self.rect.x += 7 #Ajuste de posicion de la imagen
            
        elif numero_vidas == 1:
            self.image=pygame.image.load("vida1.png")# Carga una imagen, que muestra 1 vida
            self.rect.x += 2 #Ajuste de posicion de la imagen
            
        else:
            self.image=pygame.image.load("vida0.png")# Carga una imagen, que muestra 0 vidas
            self.rect.x += -5 #Ajuste de posicion de la imagen

        
#Juego
class Game(object):
    def __init__(self):
        
        self.pantalla = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT]) #Pon 'pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT], pygame.FULLSCREEN)' 
                                                                                                  #para para que el juego ocupe toda la pantalla 
                                                                            
                                                                                                
        self.fondo = ([pygame.image.load("panel_derecho.png").convert(), pygame.image.load("carretera.png").convert(), pygame.image.load("game_over.png").convert()])#Carga la imagen de fondo inicial
        #Arriba carga tres imagenes distintas del fondo, la carretera, irlas intercalando permitira crear la sensacion de que se desplaza el escenario
        
        self.movimiento_pixeles_carretera = 0 #Permitira elegir cual imagen del fondo se selecciona
        self.timer_rango = 0 #Permitira aplazar el tiempo de eleccion de la imagen de fondo
        
        self.choque = pygame.mixer.Sound("choque.ogg")  #Carga el sonido por choque
        
        self.todos_sprites = pygame.sprite.Group() #Crear una lista de todos los sprites, esta variable facilitara que sean dibujados en la pantalla
      
        
        self.jugador=Player()
        self.todos_sprites.add(self.jugador)  #ANade al jugador a la lista de sprites
        self.vidas=Sprites() 
        self.todos_sprites.add(self.vidas) #ANade las vidas a la lista de sprites
   
   
        self.limites_izquierda_carretera = (50, 190)# Variable que ayudara a ajustar la posicion inicial de los enemigos
        self.limites_derecha_carretera = (190, 370) # Variable que ayudara a ajustar la posicion inicial de los enemigos
       
        self.enemy=[0]*N_MAX_ENEMIGOS
            
        self.nombre_foto= ("cocheazul.png", "cocherojo.png", "camion.png", "Bus.png", "policia.png", "policiaotro.png", "bombero.png", "ambulancia.png", "camion2.png", "camion3.png", "coche4.png", "furgoneta.png","taxi.png", "tanque.png") #Nombres de las imagenes que se cargaran en self.enemy[i] para definir el vehiculo del enemigo
        self.ancho=(          150,                150,            200,       200,          150,         150,        200,            200,               200,          200,          150,             150,         130,          200) #Anchos de las imagenes que se cargaran en self.enemy[i] para escalar las imagenes de cada vehiculo
        self.largo = (       75,                75,               75,        75,          75,          80,          80,              80,               80,           80,           75,              75,          75,            90) #Largos de las imagenes que se cargaran en self.enemy[i] para escalar las imagenes de cada vehiculo
        self.numero_tipoenemigos=13 #Numero de fotos de enemigos cargados -1. Se resta 1 para dejar el tanque.png aparte hasta que se obtenga un puntaje determinado en la funcion 'actualiza_puntaje( '
        
        self.posicion_inicial_enemigos = -266
        self.movimiento_enemigo = MOVIMIENTO_ENEMIGO
        
        self.num_vidas=3 #Numero inicial de vidas
      
        self.jugador_invulnerable = 0 #Controla la invulnerabilidad temporal del jugador, inicialmente no esta activa
        self.parpadeo = 0 # Controla el parpadeo intermitente tras un choque del jugador, inicialmente no esta activa
        
        self.pantalla_GameOver= 0 # Cuando este activada se prepara
        
        self.fuente = pygame.font.SysFont('Consolas', 50, True, False)
        self.puntaje = 0 #Numero que se mostrara en pantalla
        self.texto_puntaje = self.fuente.render(str(self.puntaje),True, NEGRO)
        
       
        
        for j in range (N_MAX_ENEMIGOS):  #Crea todos los enemigos
        #Para que la posicion de los enemigos sea aleatoria pero manteniendo a los vehiculos en los carriles,se fijaron dos margenes de aparicion de enemigo
        #limites izquierda y derecha
            
            rango_inicial_enemigos = random.choice(self.limites_izquierda_carretera)
            posicion_rango = self.limites_izquierda_carretera.index(rango_inicial_enemigos)
            rango_final_enemigos = self.limites_derecha_carretera[posicion_rango]   
            posicion_nombredelafoto= random.randrange(0,self.numero_tipoenemigos) #Se prepara un rango aleatorio para la eleccion de la foto del tipo de vehiculo
            self.enemy[j]=Enemy(self.nombre_foto[posicion_nombredelafoto], self.largo[posicion_nombredelafoto], self.ancho[posicion_nombredelafoto]) #Se crea el enemigo, asignandole la foto y la anchura a la que se escala
            self.enemy[j].rect.x = random.randrange(rango_inicial_enemigos, rango_final_enemigos) #Se elige una posicion entre los dos margenes de aparicion
            self.enemy[j].rect.y= self.posicion_inicial_enemigos   #El enemigo estara inicialmente en esta posicion de la pantalla
            self.todos_sprites.add(self.enemy[j]) #Mete al enemigo en la lista de todos sprites
        
      
    
    def accion_jugador(self, key):
    #Permite mover al jugador. La variable key sera un array de tres elementos, en el se leeran las teclas del raton
    #Se clica la tecla izquierda del raton? key vale (1,0,0)  Se clica la central? key vale (0,1,0) Se clica la derecha? key vale (0,0,1) Se clican todas a la vez? key vale (1,1,1)
        
        if key[0] == 1:  #Se clico la tecla izquierda del raton
            self.jugador.move(-MOVIMIENTO_JUGADOR) #Mueve al jugador 8 pixeles a la izquierda
        if key[2] == 1: #Se clico la tecla derecha del raton
            self.jugador.move(MOVIMIENTO_JUGADOR) #Mueve al jugador 8 pixeles a la derecha
        
            
    def permitir_movimiento_enemigo(self,  control_movimiento_enemigo ): 
        
        self.enemy[control_movimiento_enemigo].pausado = 0 #Permite que el enemigo elegido pueda moverse
   
              
    def acciones_enemigas(self):  
       
        for k in range(N_MAX_ENEMIGOS):
            
            if self.enemy[k].pausado == 0: #Comprueba si el enemigo debe estar pausado o no
                
                self.enemy[k].move(self.movimiento_enemigo) #Llama a la funcion que hara que el enemigo baje de posicion en la pantalla, moviendolo

                
                # COMPROBAR COLISION
                
                if (self.enemy[k].colision(self.jugador) == 1 and self.jugador_invulnerable == 0): #Se ha chocado el jugador y no esta activa la invulnerabilidad?
                    
                    self.choque.play() #suena sonido de choque
                    self.num_vidas -= 1 #Baja una vida
                    self.vidas.recarga(self.num_vidas) #Actualiza el sprite del numero de vidas
                    
                    
                    if self.num_vidas == 0: #GAME OVER #Se perdieron todas las vidas?
                        
                        pygame.mixer.music.stop() #Para la musica
                        self.todos_sprites.remove(self.jugador)  #Quita al jugador de la lista de sprites, con esto no sera representado en la pantalla
                        self.jugador_invulnerable = 1 #Bloquea el acceso a COMPROBAR COLISION (y por tanto a este if)
                        self.pantalla_GameOver = 1 #informa e ha activado la pantalla GameOver
                        pygame.time.set_timer(MOVIMIENTO_CARRETERA , 0) #Dejara de moverse el fondo de la carretera
                        self.posicion_inicial_enemigos = 700 #Enemigos apareceran abajo
                        self.movimiento_enemigo *= -1 #Enemigos se moveran de abajo a arriba
                              
                        if k == N_MAX_ENEMIGOS -1: #K es el ultimo enemigo?
                            i=0
                            h=1
                        elif k + 1 == N_MAX_ENEMIGOS -1:#K es el penultimoenemigo?
                            i = k +1
                            h = 0
                        else:
                            i = k+1
                            h = k+2
                        
                        for j in range(N_MAX_ENEMIGOS): #Recoloca casi todos los enemigos. No todos para evitar confusiones visuales
                            if (j != k and j != i and j != h ):
                                self.enemy[j].rect.y = 760
                                
                        return self.pantalla_GameOver# informa del GameOver
                        
                    else:  #Inmunidad temporal
                        pygame.time.set_timer(INMUNIDAD_COLISION, 3000) #La inmunidad temporal tras colision durara 3 segundos
                        pygame.time.set_timer(PARPADEO, 300) #El jugador se hara visible a invisible intermitentemente, durando 0.3 segundos en cada estado
                        self.jugador_invulnerable = 1 #El jugador no puede perder vidas
                    
    
                if (self.movimiento_enemigo == MOVIMIENTO_ENEMIGO and self.enemy[k].rect.y > SCREEN_HEIGHT + 30) or (self.movimiento_enemigo != MOVIMIENTO_ENEMIGO and self.enemy[k].rect.y < -SCREEN_HEIGHT + 30): #Se fue de la pantalla el enemigo?
            
             
                    self.todos_sprites.remove(self.enemy[k]) #Quita ese enemigo de la lista de sprites, esto permitira que al ser recolocado pueda tener un dibujo distinto
                    #Lo siguiente es 'recrear' ese enemigo que se fue de la pantalla:
                    
                    rango_inicial_enemigos = random.choice(self.limites_izquierda_carretera)
                    posicion_rango = self.limites_izquierda_carretera.index(rango_inicial_enemigos)
                    rango_final_enemigos = self.limites_derecha_carretera[posicion_rango]                           
                    posicion_nombredelafoto= random.randrange(0,self.numero_tipoenemigos)     #Se prepara un rango aleatorio para la eleccion de la foto del tipo de vehiculo
                    self.enemy[k] = Enemy(self.nombre_foto[posicion_nombredelafoto], self.largo[posicion_nombredelafoto], self.ancho[posicion_nombredelafoto])#Se crea el enemigo, asignandole la foto y la anchura a la que se escala
                    self.enemy[k].rect.x = random.randrange(rango_inicial_enemigos, rango_final_enemigos)   #Se elige una posicion entre los dos margenes de aparicion
                    self.enemy[k].rect.y= self.posicion_inicial_enemigos   #El enemigo estara inicialmente en esta posicion de la pantalla     
                    self.todos_sprites.add(self.enemy[k]) #Mete al enemigo en la lista de todos sprites
                             
                    self.enemy[k].pausado = 1 #El enemigo no se movera por el momento
                    
                    
        return self.pantalla_GameOver #informa si hay gameOver
            
    def actualiza_puntaje(self):
        self.puntaje +=20 #Suma 10 puntos cada vez que se llama a esta funcion
        self.texto_puntaje = self.fuente.render(str(self.puntaje),True, NEGRO) #Actualiza el dibujo del puntaje
        
        if  self.puntaje >300: #Se alcanzaron mas de 300 puntos?
            self.numero_tipoenemigos=14 #ANade al juego al ultimo elemento del array de imagenes de enemigos, el tanque
                    
    def display(self):
            
        self.todos_sprites.draw(self.pantalla) #Dibuja en la pantalla los sprites de la lista de sprites
              
        if self.pantalla_GameOver == 0: #No se activo la pantalla de game_over?
            if self.puntaje < 100:
                self.pantalla.blit(self.texto_puntaje, [600, 245])  #Pone el contador de puntaje a la derecha de la pantalla
            elif self.puntaje < 1000:
                self.pantalla.blit(self.texto_puntaje, [585, 245]) #Pone el contador un poco mas a la izquierda
            else:
                self.pantalla.blit(self.texto_puntaje, [570, 245]) #Pone el contador un poco mas a la izquierda
        elif self.puntaje <100:
            self.pantalla.blit(self.texto_puntaje, [345, 430]) #Pone el contador de puntaje en el centro
        elif self.puntaje < 1000:
            self.pantalla.blit(self.texto_puntaje, [335, 430]) #Pone el contador de puntaje en el centro pero un poco mas a la izquierda
        else:
            self.pantalla.blit(self.texto_puntaje, [325, 430]) #Pone el contador de puntaje en el centro pero un poco mas a la izquierda
            
        pygame.display.update() #Actualiza la pantalla, dibujando definitivamente los cambios antes dichos
        
        if (self.pantalla_GameOver == 0): #Si no hay Game Over
            self.pantalla.blit(self.fondo[0], [0, 0]) #Actualiza el fondo, debe actualizarse TRAS la funcion anterior, .update()
            self.pantalla.blit(self.fondo[1], [0,  +self.movimiento_pixeles_carretera -100]) #Actualiza el fondo de la carretera
        
        else: 
            self.pantalla.blit(self.fondo[2], [0, 0]) #Pon fondo de Game Over
                
            
            
# Programa principal 

def main():
    pygame.init() #Necesario para que funcione el pygame
    
    pygame.mixer.music.load("musica_fondo.mp3") #carga la musica de fondo
     
    pygame.display.set_caption("survival-motor")  #Nombre de la ventana
    
    cerrar_juego = 0 #Mientras sea distinto de 1 el juego no estara cerrado
    
    reloj = pygame.time.Clock() #Activa el reloj de Pygame
    
    
    
    while 1:
    
        if cerrar_juego == 1:
            break
        
       
        cerrar_juego = 0 
        tecla_esta_pulsada = 0 
        control_movimiento_enemigo = 0 #Indice, en el array del enemigo que sera movido
        parpadeante = 0
        game_over = 0
      
        
        pygame.mixer.music.play(-1) #Comienza a sonar la musica de fondo. El '-1' indica que cuando se acabe, vuelva a sonar
        juego=Game()
        
        #Los timer son eventos que se activan pasado un tiempo determinado
        pygame.time.set_timer(TIEMPO_ENTRE_ENEMIGOS , 3000) #3 segundos de tiempo inicial entre enemigos
        pygame.time.set_timer(PUNTAJE , 3000)  #3 segundos para recibir puntaje
        
        while cerrar_juego == 0: 
            for event in pygame.event.get(): 
                if event.type == pygame.QUIT:  #Se ha hecho clic en la 'x' de la pestaNa?
                    cerrar_juego = 1
                    break
                               
                elif (event.type == TIEMPO_ENTRE_ENEMIGOS ): # Se cumplio el tiempo de espera para aNadir un enemigo?
                 
                    juego.permitir_movimiento_enemigo(control_movimiento_enemigo) #Hara moverse al enemy[i] con i = control_movimiento_enemigo
                    control_movimiento_enemigo += 1#Se prepara para acceder a la posicion [i] del siguiente enemigo a mover
                
                    if control_movimiento_enemigo == N_MAX_ENEMIGOS: #Ya no hay mas enemigos que mover?
                        control_movimiento_enemigo = 0 #Se prepara para acceder a la posicion enemy.[0]
                        
                    pygame.time.set_timer(TIEMPO_ENTRE_ENEMIGOS , random.randrange(INTERVALO_ENEMIGOS_INFERIOR, INTERVALO_ENEMIGOS_SUPERIOR) ) #El tiempo entre enemigos es aleatorio, entre 0.8 y 1.5 segundos
                    
                if (event.type == INMUNIDAD_COLISION): #Ha pasado el tiempo de inmunidad tras colision?
                
                    pygame.time.set_timer(INMUNIDAD_COLISION, 0) #Apaga el timer de inmunidad por colision
                    pygame.time.set_timer(PARPADEO, 0) #Apaga el timer que controla el parpadeo
                    juego.jugador_invulnerable =  parpadeante = 0 #Anula las variables que controlaban la inmunidad y el parpadeo
                    juego.todos_sprites.add(juego.jugador) #Se asegura que el jugador sea visible tras el parpadeo
            
            
                if (event.type == PARPADEO): #Ha pasado el tiempo de intermitencia?
                #La funcion quitara y metera al jugador en la lista de sprites de manera intermitente
                    if parpadeante == 0:
                        juego.todos_sprites.remove(juego.jugador)
                        parpadeante = 1
                    else:
                        juego.todos_sprites.add(juego.jugador)
                        parpadeante = 0
                               
                if event.type == PUNTAJE:  #Paso el tiempo para aNadir el puntaje?
                    juego.actualiza_puntaje()
                    
                    
                if game_over == 1 and raton[1] == 1: #Se esta en game_over y se pulso el boton central del raton?
                        cerrar_juego = 2 #Permite salir del bucle del juego pero sin cerrarlo, haciendo que se ejecute de nuevo
                
                    
                
            
            juego.movimiento_pixeles_carretera += MOVIMIENTO_PIXELES_CARRETERA #Mueve pixeles la imagen de la carretera para dar movilidad
                            
            if juego.movimiento_pixeles_carretera >=100 :
                juego.movimiento_pixeles_carretera = 0     #Reinicia los pixeles movidos de la imagen de la carretera
                
                
            raton=pygame.mouse.get_pressed() #Recibe el tecleo del raton
            juego.accion_jugador(raton) #Manda el tecleo del raton a la funcion que permitira mover al jugador
            game_over = juego.acciones_enemigas() #La variable se pondra 1 al perder todas las vidas
            
            if game_over == 1: #Fin del juego?
                pygame.time.set_timer(PUNTAJE , 0) #Detiene el contador del puntaje
            
            juego.display()#Llama a la funcion que dibujara todo en la pantalla
          
            reloj.tick(50) #fps, mientras menor sea mas lento se moveran los sprites, mientras mas alto sea mas rapido se moveran los sprites
    
    pygame.quit() #Necesario para cerrar el programa sin errores
    
    
    
    
#Ejecucion del programa    
main()